<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Accueil</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet"  href="./css/style.css">
</head>

<style>
    td.fit,
    th.fit {
        white-space: nowrap;
        width: 1%;
    }
</style>
<body>

    <jsp:include page="filter.jsp"/>

    <c:choose>
        <c:when test="${sessionScope.userConnected!=null}">
            <div class="container-fluid" style="width: 70%">
                <br>
                <h3>Liste  des livres de la bibliothèque :</h3>
                <br>
                <table id="mainTable" class="table table-hover table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th style="width: 25%" scope="col">Titre</th>
                        <th style="width: 20%" scope="col">Auteur</th>
                        <th style="width: 20%" scope="col">Type</th>
                        <th style="width: 20%" scope="col">Exemplaires</th>
                        <th style="width: 15%" scope="col">Disponibilité</th>
                    </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${publicBooks}" var="book">
                            <tr>
                                <td>${book.title}</td>
                                <td>${book.author}</td>
                                <td>${book.type}</td>
                                <td>${book.copies}</td>
                                <td >${book.disponibility}</td>
                                <c:choose>
                                    <c:when test="${book.disponibility=='Oui'}">
                                        <td>
                                            <a href='${pageContext.request.contextPath}/loans?action=toLoan&user=${sessionScope.userConnected.username}&book=${book.idBook}'>
                                                <button formaction="" class="btn-dark" >Emprunter</button>
                                            </a>
                                       </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td class="text-center"><button class="btn btn-info" disabled>Indispo</button></td>
                                    </c:otherwise>
                                    </c:choose>

                            </tr>
                        </c:forEach>

                    </tbody>
                </table>
                <div class="row justify-content-center">
                    <a href='${pageContext.request.contextPath}/logout'>
                        <button formaction="" class="btn-dark" >Déconnection</button>
                    </a>
                </div>

            </div>
        </c:when>
        <c:otherwise>
            <div class="container-fluid" style="width: 70%">
                <br>
                <h3>Liste  des livres de la bibliothèque :</h3>
                <br>
                <table id="mainTable1" class="table table-hover table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th style="width: 25%" scope="col">Titre</th>
                        <th style="width: 20%" scope="col">Auteur</th>
                        <th style="width: 20%" scope="col">Type</th>
                        <th style="width: 20%" scope="col">Exemplaires</th>
                        <th style="width: 15%" scope="col">Disponibilité</th>
                    </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${publicBooks}" var="book">
                            <tr>
                                <td>${book.title}</td>
                                <td>${book.author}</td>
                                <td>${book.type}</td>
                                <td>${book.copies}</td>
                                <td>${book.disponibility}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>

            </div>

            <div class="row justify-content-center">
                <div class="col-2">
                    <a style="margin-left: 25%" href="${pageContext.request.contextPath}/logIn.jsp" class="btn btn-primary stretched-link">Se connecter</a>
                </div>
            </div>
            <script>
                function filtreTitre() {
                    var input, filter, table, tr, td, i, txtValue;
                    input = document.getElementById("Titre");
                    filter = input.value.toUpperCase();
                    table = document.getElementById("mainTable1");
                    tr = table.getElementsByTagName("tr");
                    for (i = 0; i < tr.length; i++) {
                        td = tr[i].getElementsByTagName("td")[0];
                        if (td) {
                            txtValue = td.textContent || td.innerText;
                            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                                tr[i].style.display = "";
                            } else {
                                tr[i].style.display = "none";
                            }
                        }
                    }
                }

                function filtreAuteur() {
                    var input, filter, table, tr, td, i, txtValue;
                    input = document.getElementById("Auteur");
                    filter = input.value.toUpperCase();
                    table = document.getElementById("mainTable1");
                    tr = table.getElementsByTagName("tr");
                    for (i = 0; i < tr.length; i++) {
                        td = tr[i].getElementsByTagName("td")[1];
                        if (td) {
                            txtValue = td.textContent || td.innerText;
                            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                                tr[i].style.display = "";
                            } else {
                                tr[i].style.display = "none";
                            }
                        }
                    }
                }

                function filtreDispo() {
                    var input, filter, table, tr, td, i, txtValue;
                    input = document.getElementById("Dispo");
                    filter = input.value.toUpperCase();
                    table = document.getElementById("mainTable1");
                    tr = table.getElementsByTagName("tr");
                    for (i = 0; i < tr.length; i++) {
                        td = tr[i].getElementsByTagName("td")[4];
                        if (td) {
                            txtValue = td.textContent || td.innerText;
                            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                                tr[i].style.display = "";
                            } else {
                                tr[i].style.display = "none";
                            }
                        }
                    }
                }

                function filtreType() {
                    var input, filter, table, tr, td, i, txtValue;
                    input = document.getElementById("Type");
                    filter = input.value.toUpperCase()
                    table = document.getElementById("mainTable1");
                    tr = table.getElementsByTagName("tr");
                    for (i = 0; i < tr.length; i++) {
                        td = tr[i].getElementsByTagName("td")[2];
                        if (td) {
                            txtValue = td.textContent || td.innerText;
                            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                                tr[i].style.display = "";
                            } else {
                                tr[i].style.display = "none";
                            }
                        }
                    }
                }

            </script>
        </c:otherwise>
    </c:choose>









<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
