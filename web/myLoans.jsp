<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<html>
<head>
    <title>Mes empreints</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

</head>
<body>


    <c:choose>
        <c:when test="${sessionScope.userConnected!=null}">
        <h3 class="text-center" style="color: red"> Bienvenue : ${sessionScope.userConnected.username}</h3>
        <div class="container-fluid" style="width: 70%">
            <br>
            <h3>Mes empreints</h3>
            <br>
            <table id="myLoans" class="table table-hover table-striped">
                <thead class="thead-dark">
                <tr>
                    <th style="width: 25%" scope="col">Date</th>
                    <th style="width: 20%" scope="col">Livre</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${myLoans}" var="loan">
                    <tr>
                        <td>${loan.date}</td>
                        <td>${loan.book.title}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <form action="${pageContext.request.contextPath}/home" method="get">
            <div class="row justify-content-center">
                <div class="col-2">
                    <div class="row justify-content-center">
                        <a href="${pageContext.request.contextPath}/home" class="btn btn-warning stretched-link">Retour à l'accueil</a>
                    </div>
                </div>
            </div>
        </form>

        </c:when>
            <c:otherwise>
                <c:redirect url = "logIn.jsp"/>
            </c:otherwise>
    </c:choose>










    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
