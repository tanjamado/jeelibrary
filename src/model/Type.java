package model;

public enum Type {
    THRILLER,
    POLICIER,
    ROMANCE,
    SF,
    AVENTURE;

    public String getName() {
        return name();
    }

}
