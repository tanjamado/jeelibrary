package dao;

import model.User;
import util.HibernateUtil;

import javax.persistence.EntityManager;
import java.util.Optional;

public class UserDAO {

    private static EntityManager em  = HibernateUtil.emInit();

    public boolean loginIn(String username, String password)  {

        if (username == null || password == null) {
            return false;
        }

        Optional<User> optionalUser = em.createQuery("select u from User u where u.username = :username", User.class)
                .setParameter("username", username)
                .getResultList()
                .stream()
                .findFirst();

        if (!optionalUser.isPresent()) {
            return false;
        }
        User u = optionalUser.get();
        if (! password.equals(u.getPassword())) {
            return false;
        }
        return true;



    }

    public Optional<User> getUserByName(String username) {
        return em.createQuery("select u from User u where u.username = :username", User.class)
                .setParameter("username", username)
                .getResultList()
                .stream()
                .findFirst();
    }
}
